# MMOD Panel Authors

This file lists the authors of - and contributors to - this project.

## The List

| From | Contributor |
| :---: | :---: |
| 2014 | [Richard Winters](mailto:kirvedx@gmail.com) |
| 2015 | [Jonatan Zeidler](mailto:jonatan_zeidler@gmx.de) |
| 2021 | Joel Denning [@joeldenning](https://gitlab.com/joeldenning) |