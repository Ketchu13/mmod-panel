/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import gulpSass from 'gulp-sass';                   // Again, obviously!
import sass from 'sass';                            // We have to declare a default compiler for gulp-sass
import sassGlob from 'gulp-sass-glob';              // For special globbing support?
import plumber from 'gulp-plumber';                 // For helping gulp manipulate webpack
import watch from 'gulp-watch';                     // For Auto-Compilation/Transpilation upon save
import del from 'del';                              // Used by our clean process, for the most part -
import bump from 'gulp-bump-version';               // Used for version increment automation
import zip from 'gulp-zip';                         // For compressing the extension directory


// GLOBALS
const defSass = gulpSass( sass );

const args =                                            // Allows us to accept arguments to our gulpfile
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption )
            {
                // argument value
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check if we supplied arguments for updating the version of the project and its files
let bumpVersion         = args['bump-version'],
    toVersion           = args['to-version'],
    byType              = args['type'] || 'patch',
    bumpOptions         = ( ( bumpVersion ) ? ( ( toVersion ) ? { version: toVersion } : { type: byType } ) : { } ),
    prefVerBumpOptions  = ( ( bumpVersion ) ? ( ( toVersion ) ? { version: toVersion } : { type: byType } ) : { } ),
    metaVerBumpOptions  = ( ( bumpVersion ) ? ( ( toVersion ) ? { version: toVersion } : { type: byType } ) : { } ),
    projectBumpOptions  = ( ( bumpVersion ) ? ( ( toVersion ) ? { version: toVersion } : { type: byType } ) : { } );

prefVerBumpOptions.key = `const mver = '`;
metaVerBumpOptions.key = `since version `;
projectBumpOptions.key = `"version": "`;

// Bump the version in our projects files (called manually, or through the build-release task)
gulp.task
(
    'bump-file-versions',
    () =>
    {
        // Otherwise, by providing --bump-version only, you will increment the patch revision

        // Start with all the files using the typical key, then hit out package.json: 'dist/src/**/*.js',
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.js',
                '**/*.scss',
                '**/*.po',
                '!node_modules{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bump the version in our in our package.json file
gulp.task
(
    'bump-pref-file-version',
    () =>
    {
        // Target our package.json:
        return gulp.src
        (
            '**/*.js',
            { base: './' }
        )
        .pipe( bump( prefVerBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bump the version in our in our package.json file
gulp.task
(
    'bump-meta-file-version',
    () =>
    {
        // Target our package.json:
        return gulp.src
        (
            '**/*.json',
            { base: './' }
        )
        .pipe( bump( metaVerBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bump the version in our in our package.json file
gulp.task
(
    'bump-project-version',
    () =>
    {
        // Target our package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Compiles SCSS files
gulp.task
(
    'compile-scss',
    () =>
    {
        if( args.dev )
        {
            return gulp.src( 'scss/stylesheet.scss' )
            .pipe(plumber())
            .pipe( sourcemaps.init() )
            .pipe( sassGlob() )
            .pipe
            (
                defSass( { outputStyle: 'expanded', includePaths: ['node_modules'] } ).on( 'error', defSass.logError )
            )
            .pipe( sourcemaps.write( '.' ) )  // Remove '.' for inline scss sourcemaps
            .pipe( gulp.dest( 'build/prep' ) );
        }

        return gulp.src( 'scss/stylesheet.scss' )
        .pipe(plumber())
        .pipe( sassGlob() )
        .pipe
        (
            defSass( { outputStyle: 'expanded', includePaths: ['node_modules'] } ).on( 'error', defSass.logError )
        )
        .pipe( gulp.dest( 'build/prep' ) );
    }
);


// Watches for changes in SCSS files
gulp.task
(
    'watch',
    () =>
    {
        gulp.watch( 'scss/**/*.scss', ['compile-sass'] );
    }
);


// Copies extension files to a staging directory
gulp.task
(
    'copy-extension-files',
    () =>
    {
        return gulp.src( ['src/**/*', 'data/**/*', '!data/ui/**/*', '!data/ui', 'AUTHORS.md', 'LICENSE.md', 'CHANGELOG.md', 'INSTALL.md', 'NEWS.md', 'README.md'] )
        .pipe( gulp.dest( 'build/prep' ) );
    }
);


// Compresses staging directory to produce an uploadable gnome-shell extension
gulp.task
(
    'compress-extension',
    () =>
    {
        return gulp.src( ['build/prep/**/*'] )
        .pipe( zip( 'mmod-panel.zip' ) )
        .pipe( gulp.dest( 'build/release' ) );
    }
);


// Cleans the build/dist directory (removes it).
gulp.task
(
    'clean',
    ( done ) =>
    {
        let deleteList = ( !args.alt ) ? 'build' : 'build';
        del.sync
        (
            deleteList,
            { force: true }
        );

        // While typescript compiler can just return its process,
        // for some reason del.sync() needs to trigger async
        // completion (ensure you provide the argument to the
        // anon function so that it's available):
        done();
    }
);


// Runs the build-release process.
gulp.task
(
    'build-release',
    gulp.series
    (
        'clean',
        'compile-scss',
        'copy-extension-files',
        'compress-extension',
        ( done ) =>
        {
            done();
        }
    )
);


// Runs the build-versioned-release process.
gulp.task
(
    'build-versioned-release',
    gulp.series
    (
        //'clean',
        'bump-project-version',
        'bump-pref-file-version',
        'bump-meta-file-version',
        'bump-file-versions',
        //'compile-scss',
        //'copy-extension-files',
        //'compress-extension',
        ( done ) =>
        {
            done();
        }
    )
);


// Runs default build process.
gulp.task
(
    'default',
    gulp.series
    (
        'compile-scss',
        'copy-extension-files',
        ( done ) =>
        {
            done();
        }
    )
);


