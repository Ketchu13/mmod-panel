/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// Switching it up to use GTK4 where applicable!
const config                    = imports.misc.config;
const GNOME_SHELL_VERSION_PARTS = config.PACKAGE_VERSION.split( '.' ),
      SHELL_VERSION_MAJOR       = Number.parseInt( GNOME_SHELL_VERSION_PARTS[0] ),
      SHELL_VERSION_MINOR       = Number.parseInt( GNOME_SHELL_VERSION_PARTS[1] ),
      USE_GTK4                  = !( SHELL_VERSION_MAJOR < 40 );

// Load from gi namespace
const {
    GObject,
    Gtk,
    Gdk
} = imports.gi;

// Load the lang namespace
const { lang } = imports;

// Load extension utils
const { extensionUtils } = imports.misc;

// Prepare a context for our extension
const our = extensionUtils.getCurrentExtension();

// Prepare GetText
const GETTEXT_EXT_DOMAIN = imports.gettext.domain( 'mmod-panel' );
const _ = GETTEXT_EXT_DOMAIN.gettext;

// Prepare things for our preferences to top off
const sver = config.PACKAGE_VERSION.split( "." ).map( function ( x ) { return + x; } );

const mver = '13.0.0';

const schema = "org.gnome.shell.extensions.mmod-panel";

//Edges
const EDGE_TOP      = 0;
const EDGE_BOTTOM   = 1;
const EDGE_LEFT     = 2;
const EDGE_RIGHT    = 3;

//const RESETCOLOR = 'rgba(0,0,0,0)';
//const BLACKCOLOR = 'rgba(0,0,0,1)';
const ICON_LOGO = our.path + '/res/img/mico/mmod-plain-96-20.png';
const ICON_M4IKEN = our.path + '/res/img/mico/mstar/mmod-logo-fin-24.png';
const ICON_M4IKEN_RED = our.path + '/res/img/mico/mstar/mmod-logo-red-24.png';
const ICON_DONATE = our.path + '/res/img/ppico/donate/btn_donate_LG.gif';
const ICON_GMAIL = our.path + '/res/img/gico/gmail/gmail-24.png';
const ICON_EMAIL = our.path + '/res/img/generic/email/email-24.png';
const ICON_GNOME = our.path + '/res/img/gnico/gnome/gnome-24.png';
const ICON_GLOOK = our.path + '/res/img/gnico/gnome/gnome-24.png';


if( USE_GTK4 )
{
    // Load newly required CSS support:
    let provider = new Gtk.CssProvider();

    provider.load_from_path( our.dir.get_path() + '/prefs.css');

    Gtk.StyleContext.add_provider_for_display(
        Gdk.Display.get_default(),
        provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    );
}


const MMODPanelBuilderScope = GObject.registerClass
(
    {
        Implements: [Gtk.BuilderScope],
    },
    class MyBuilderScope extends GObject.Object
    {

        vfunc_create_closure( builder, handlerName, flags, connectObject )
        {
            if( flags & Gtk.BuilderClosureFlags.SWAPPED )
                throw new Error( 'Unsupported template signal flag "swapped"' );

            if( typeof this[handlerName] === 'undefined' )
                throw new Error( `${handlerName} is undefined` );

            return this[handlerName].bind(connectObject || this);
        }
    }
);


function init() {}


function fillPreferencesWindow( window )
{
    let builder = Gtk.Builder.new();

    builder.set_scope( new MMODPanelBuilderScope() );
    builder.set_translation_domain( 'mmod-panel' );
    builder.add_from_file( our.path + '/ui/preferences.ui' );

    let page = builder.get_object( 'prefsWindow' );

    window.add( page );
    window.set_default_size( 800, 600 );
    window.search_enabled = true;
}
