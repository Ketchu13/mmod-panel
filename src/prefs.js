/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2015 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// Switching it up to use GTK4 where applicable!
const config                    = imports.misc.config;
const GNOME_SHELL_VERSION_PARTS = config.PACKAGE_VERSION.split( '.' ),
      SHELL_VERSION_MAJOR       = Number.parseInt( GNOME_SHELL_VERSION_PARTS[0] ),
      SHELL_VERSION_MINOR       = Number.parseInt( GNOME_SHELL_VERSION_PARTS[1] ),
      USE_GTK4                  = !( SHELL_VERSION_MAJOR < 40 ),
      USE_CONVENIENCE           = !USE_GTK4 && SHELL_VERSION_MINOR < 36;

//if( USE_GTK4 )
//{
    // Load from gi namespace:
const {
    Gio,
    Gdk,
    GdkPixbuf,
    GObject,
    Gtk,
    GLib
} = imports.gi;

// Load the lang namespace:
const { lang } = imports;

// Load extension utils::
const { extensionUtils } = imports.misc;

//}
//else
/*{
    // Load from gi namespace
    const Gio = imports.gi.Gio;
    const Gdk = imports.gi.Gdk;
    const GdkPixbuf = imports.gi.GdkPixbuf;
    const GObject = imports.gi.GObject;
    const Gtk = imports.gi.Gtk;
    const GLib = imports.gi.GLib;

    // Load from lang
    const lang = imports.lang;

    // Load extension utils: [TODO: REMOVE?]:
    const extensionUtils = imports.misc.extensionUtils;
}*/

// Prepare a context for our extension:
const our = extensionUtils.getCurrentExtension();

// Prepare GetText
const GETTEXT_EXT_DOMAIN = imports.gettext.domain( 'mmod-panel' );
const _ = GETTEXT_EXT_DOMAIN.gettext;

// Prepare the convenience namespace for loading settings:
const please = our.imports.convenience;

//const lib = our.imports.lib;

// ...and use it:
const sver = config.PACKAGE_VERSION.split( "." ).map( ( x ) => { return + x; } );

const mver = '13.0.0';

const schema = "org.gnome.shell.extensions.mmod-panel";

//Edges
const EDGE_TOP      = 0;
const EDGE_BOTTOM   = 1;
const EDGE_LEFT     = 2;
const EDGE_RIGHT    = 3;

//const RESETCOLOR = 'rgba(0,0,0,0)';
//const BLACKCOLOR = 'rgba(0,0,0,1)';
const ICON_LOGO = our.path + '/res/img/mico/mmod-plain-96-20.png';
const ICON_M4IKEN = our.path + '/res/img/mico/mstar/mmod-logo-fin-24.png';
const ICON_M4IKEN_RED = our.path + '/res/img/mico/mstar/mmod-logo-red-24.png';
const ICON_DONATE = our.path + '/res/img/ppico/donate/btn_donate_LG.gif';
const ICON_GMAIL = our.path + '/res/img/gico/gmail/gmail-24.png';
const ICON_EMAIL = our.path + '/res/img/generic/email/email-24.png';
const ICON_GNOME = our.path + '/res/img/gnico/gnome/gnome-24.png';
const ICON_GLOOK = our.path + '/res/img/gnico/gnome/gnome-24.png';


if( USE_GTK4 ) {
    // Load newly required CSS support:
    const provider = new Gtk.CssProvider();

    provider.load_from_path( our.dir.get_path() + '/prefs.css');

    Gtk.StyleContext.add_provider_for_display(
        Gdk.Display.get_default(),
        provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    );
}



/**
 * Initialization procedure
 *
 * @params none
 *
 * @returns { void }
 */
function init() {
    // extensionUtils has a convenience method now that
    // is literally a clone of our historic initTranslations method:
    extensionUtils.initTranslations( 'mmod-panel' );
}


/**
 * Returns a widget to Gnome Extensions Preferences
 *
 * @params none
 *
 * @returns { any } an instance of a Preferences Widget
 */
function buildPrefsWidget() {
    let prefs = new preferences( schema );
    return prefs.buildPrefsWidget();
}


/**
 * Preferences class declaration and "constructor"
 *
 * @param { any } schema The schema for the preferences requested
 *
 * @returns { void }
 */
function preferences( schema  ) {
    this.init( schema );
}


/**
 * Preferences class (via prototype) defintion
 */
preferences.prototype =
{
    settings: null,

    init: ( schema ) => {
        // extensionUtils is the new convenience as of 3.36
        this.settings = ( !USE_CONVENIENCE ) ? extensionUtils.getSettings( schema ) : please.getSettings( schema );
    },

    buildPrefsWidget: () => {
        this.notebook = new Gtk.Notebook();
        this.notebook.set_scrollable( true );
        this.newValueAppearance = null;
        this.oldValueAppearance = null;


        this.gridGPreferences = new Gtk.Grid();
        this.gridGPreferences.set_column_homogeneous( false );
        //this.gridGPreferences.set_hexpand( false );
        this.gridGPreferences.margin = this.gridGPreferences.row_spacing = 10;
        this.gridGPreferences.column_spacing = 2;

        this.scrollWindowGPreferences = new Gtk.ScrolledWindow(
            {
                'hscrollbar-policy': Gtk.PolicyType.AUTOMATIC,
                'vscrollbar-policy': Gtk.PolicyType.AUTOMATIC,
                'hexpand': true, 'vexpand': true, 'visible': true
            }
        );

        if( USE_GTK4 )
            this.scrollWindowGPreferences.set_child( this.gridGPreferences );
        else {
            this.scrollWindowGPreferences.add( this.gridGPreferences );     // add_with_viewport has been deprecated since 3.8 (https://developer.gnome.org/gtk3/stable/GtkScrolledWindow.html#gtk-scrolled-window-add-with-viewport)
            this.scrollWindowGPreferences.show_all();
        }

        this.labelGeneral = new Gtk.Label( { label: _( "General" ) } );    // Tab
        this.notebook.append_page( this.scrollWindowGPreferences, this.labelGeneral );


        // General panel and associated settings
        this.labelPanel = new Gtk.Label( { label: _( "MMod Panel " ), xalign: 0 } );     // Menu Item    drop-down
        this.valuePanelBox = new Gtk.Box( { hexpand: false, vexpand: false } );
        this.valuePanel = new Gtk.Switch( { active: this.settings.get_boolean( "mmod-panel-enabled" ), vexpand: false } );
        this.valuePanel.connect( 'notify::active', lang.bind( this, this.changePanelSetting ) );

        if( USE_GTK4 ) {
            this.valuePanelBox.prepend( this.labelPanel );
            this.valuePanelBox.append( this.valuePanel );
        }
        else {
            this.valuePanelBox.add( this.labelPanel );
            this.valuePanelBox.add( this.valuePanel );
        }

        this.gridGPreferences.attach( this.valuePanelBox, 1, 1, 1, 1 );


        // Panel comfort settings
        this.labelComfort = new Gtk.Label( { label: _( "Comfort level " ), xalign: 0 } );     // Menu Item    drop-down
        this.gridGPreferences.attach( this.labelComfort, 3, 1, 1, 1 );
        this.valueComfort = new Gtk.ComboBoxText();
        this.valueComfort.append_text( _( "Compact" ) );
        this.valueComfort.append_text( _( "Cozy" ) );
        this.valueComfort.append_text( _( "Comfortable" ) );
        this.valueComfort.set_active( this.settings.get_enum( "comfort-level" ) );
        this.valueComfort.connect( 'changed', lang.bind( this, this.changeComfortLevel ) );
        this.gridGPreferences.attach( this.valueComfort, 4, 1, 2, 1 );


        // Panel position setting
        this.labelPanelPosition = new Gtk.Label( { label: _( "Position " ), xalign: 1 } );
        this.gridGPreferences.attach( this.labelPanelPosition, 3, 2, 1, 1 );
        this.valuePanelPosition = new Gtk.ComboBoxText();
        this.valuePanelPosition.append_text( _( "Bottom" ) );
        this.valuePanelPosition.append_text( _( "Top" ) );
        this.valuePanelPosition.set_active( this.settings.get_enum( "panel-position" ) );
        this.valuePanelPosition.connect( 'changed', lang.bind( this, this.changePanelPosition ) );
        this.gridGPreferences.attach( this.valuePanelPosition, 4, 2, 2, 1 );


        // Show in menu tray setting
        this.valueShowInTrayMenuBox = new Gtk.Box( { hexpand: false } );
        this.valueShowInTrayMenu = new Gtk.CheckButton( { label: _( "Menu in tray menu" ), active: this.settings.get_boolean( "show-in-tray-menu" ) } );
        this.valueShowInTrayMenu.connect( 'notify::active', lang.bind( this, this.changeShowInTrayMenu ) );

        if( USE_GTK4 )
            this.valueShowInTrayMenuBox.append( this.valueShowInTrayMenu );
        else
            this.valueShowInTrayMenuBox.add( this.valueShowInTrayMenu );

        this.gridGPreferences.attach( this.valueShowInTrayMenuBox, 4, 3, 1, 1 );


        // Add a little space between setting groups so that we don't confuse the end user
        this.pbssep = new Gtk.Box( { hexpand: false } );

        if( USE_GTK4 )
            this.pbssep.append( new Gtk.Label( { label: "\t" } ) );
        else
            this.pbssep.add( new Gtk.Label( { label: "\t" } ) );

        this.gridGPreferences.attach( this.pbssep, 1, 4, 1, 1 );


        // Panel Button and associated settings
        this.labelPanelButton = new Gtk.Label( { label: _( "Panel button " ), xalign: 0 } );    // Menu Item    switch
        this.valuePanelButtonBox = new Gtk.Box( { hexpand: false } );   // Place switch in Gtk.Box with no hexpand, allows it to align left.
        this.valuePanelButton = new Gtk.Switch( { active: this.settings.get_boolean( "panel-button-enabled" ) } );
        this.valuePanelButton.connect( 'notify::active', lang.bind( this, this.changePanelButton ) );

        if( USE_GTK4 ) {
            this.valuePanelButtonBox.prepend( this.labelPanelButton );
            this.valuePanelButtonBox.append( this.valuePanelButton );
        }
        else {
            this.valuePanelButtonBox.add( this.labelPanelButton );
            this.valuePanelButtonBox.add( this.valuePanelButton );
        }

        this.gridGPreferences.attach( this.valuePanelButtonBox, 1, 5, 1, 1 );


        // Associated menu item     Panel button icon
        this.labelPanelButtonIcon = new Gtk.Label( { label: _( "Icon " ), xalign: 1 } );     // Menu Item    drop-down
        this.gridGPreferences.attach( this.labelPanelButtonIcon, 3, 5, 1, 1 );


        // Drop down for Panel button icon
        this.valuePanelButtonIcon = new Gtk.ComboBoxText();
        this.valuePanelButtonIcon.append_text( _( "M4Iken" ) );
        this.valuePanelButtonIcon.append_text( _( "Apps" ) );
        this.valuePanelButtonIcon.append_text( _( "Debian" ) );
        this.valuePanelButtonIcon.append_text( _( "Fedora" ) );

        const cicon = this.settings.get_enum( "panel-button-icon" );
        if( cicon === 4 )
            this.valuePanelButtonIcon.append_text( _( "Custom" ) );

        this.valuePanelButtonIcon.set_active( cicon );
        this.valuePanelButtonIcon.connect( 'changed', lang.bind( this, this.changePanelButtonIcon ) );
        //this.valuePanelButtonIconBox.add( this.valuePanelButtonIcon );
        this.gridGPreferences.attach( this.valuePanelButtonIcon, 4, 5, 2, 1 );


        // Associated menu item     Custom Panel Button Icon & Preview
        this.customPanelButtonIconFilename = this.settings.get_string( "panel-button-icon-path" );
        this.dipath = this.customPanelButtonIconFilename;
        this.valueCustomPanelButtonIcon = new Gtk.Image();
        this.loadCustomPanelButtonIcon();
        this.valueCustomPanelButtonIconBox = new Gtk.Box( { hexpand: false } );
        this.valueCustomPanelButtonIcon2 = new Gtk.Button( { child: this.valueCustomPanelButtonIcon, hexpand: false } );
        this.valueCustomPanelButtonIcon2.connect( 'clicked', lang.bind( this, this.changeCustomPanelButtonIcon ) );

        if( USE_GTK4 )
            this.valueCustomPanelButtonIconBox.append( this.valueCustomPanelButtonIcon2 );
        else
            this.valueCustomPanelButtonIconBox.add( this.valueCustomPanelButtonIcon2 );

        this.gridGPreferences.attach( this.valueCustomPanelButtonIconBox, 4, 6, 2, 1 );


        // Add a little space between setting groups so that we don't confuse the end user
        this.pbissep = new Gtk.Box( { hexpand: false } );

        if( USE_GTK4 )
            this.pbissep.append( new Gtk.Label( { label: "\t" } ) );
        else
            this.pbissep.add( new Gtk.Label( { label: "\t" } ) );

        this.gridGPreferences.attach( this.pbissep, 1, 7, 1, 1 );


        // Associated menu item     Display favorites
        this.labelDisplayFavorites = new Gtk.Label( { label: _( "Display favorites " ), xalign: 0 } );    // Menu Item
        this.gridGPreferences.attach( this.labelDisplayFavorites, 1, 8, 1, 1 );
        this.valueDisplayFavoritesBox = new Gtk.Box( { hexpand: false } );
        this.valueDisplayFavorites = new Gtk.Switch( { active: this.settings.get_boolean( "display-favorites-enabled" ) } );
        this.valueDisplayFavorites.connect( 'notify::active', lang.bind( this, this.changeDisplayFavorites ) );

        if( USE_GTK4 )
            this.valueDisplayFavoritesBox.append( this.valueDisplayFavorites );
        else
            this.valueDisplayFavoritesBox.add( this.valueDisplayFavorites );

        this.gridGPreferences.attach( this.valueDisplayFavoritesBox, 4, 8, 1, 1 );


        this.valueShowRunningAppsBox = new Gtk.Box( { hexpand: false } );
        this.valueShowRunningApps = new Gtk.CheckButton( { label: _( "Show running" ), active: this.settings.get_boolean( "show-running-apps" ) } );
        this.valueShowRunningApps.connect( 'notify::active', lang.bind( this, this.changeShowRunningApps ) );

        if( USE_GTK4 )
            this.valueShowRunningAppsBox.append( this.valueShowRunningApps );
        else
            this.valueShowRunningAppsBox.add( this.valueShowRunningApps );

        this.gridGPreferences.attach( this.valueShowRunningAppsBox, 4, 9, 1, 1 );


        this.valueShowFavsBeforePrefsBox = new Gtk.Box( { hexpand: false } );
        this.valueShowFavsBeforePrefs = new Gtk.CheckButton( { label: _( "Before prefs" ), active: this.settings.get_boolean( "favorites-before-preferences" ) } );
        this.valueShowFavsBeforePrefs.connect( 'notify::active', lang.bind( this, this.changeShowFavsBeforePrefs ) );

        if( USE_GTK4 )
            this.valueShowFavsBeforePrefsBox.append( this.valueShowFavsBeforePrefs );
        else
            this.valueShowFavsBeforePrefsBox.add( this.valueShowFavsBeforePrefs );

        this.gridGPreferences.attach( this.valueShowFavsBeforePrefsBox, 4, 10, 1, 1 );


        // Add a little space between setting groups so that we don't confuse the end user
        this.ditssep = new Gtk.Box( { hexpand: false } );

        if( USE_GTK4 )
            this.ditssep.append( new Gtk.Label( { label: "\t" } ) );
        else
            this.ditssep.add( new Gtk.Label( { label: "\t" } ) );

        this.gridGPreferences.attach( this.ditssep, 1, 11, 1, 1 );


        // Associated menu item     Display date in tray area
        this.labelDateInTray = new Gtk.Label( { label: _( "Show date in tray" ), xalign: 0 } );    // Menu Item
        this.gridGPreferences.attach( this.labelDateInTray, 1, 12, 1, 1 );
        this.valueDateInTrayBox = new Gtk.Box( { hexpand: false } );
        this.valueDateInTray = new Gtk.Switch( { active: this.settings.get_boolean( "date-in-sys-tray" ) } );
        this.valueDateInTray.connect( 'notify::active', lang.bind( this, this.changeDateInTray ) );

        if( USE_GTK4 )
            this.valueDateInTrayBox.append( this.valueDateInTray );
        else
            this.valueDateInTrayBox.add( this.valueDateInTray );

        this.gridGPreferences.attach( this.valueDateInTrayBox, 4, 12, 1, 1 );


        // Tab footer / page number
        let labelSpaceGPreferences0 = new Gtk.Label( { label: "\t", xalign: 0 } );
        this.gridGPreferences.attach( labelSpaceGPreferences0, 0, 13, 1, 1 );
        let labelSpaceGPreferences2 = new Gtk.Label( { label: "\t", xalign: 0, hexpand: true } );
        this.gridGPreferences.attach( labelSpaceGPreferences2, 2, 13, 1, 1 );
        let labelSpaceGPreferences3 = new Gtk.Label( { label: "\t", xalign: 0, hexpand: false } );
        this.gridGPreferences.attach( labelSpaceGPreferences3, 3, 13, 1, 1 );
        let labelSpaceGPreferences5 = new Gtk.Label( { label: "1/3", xalign: 1 } );
        this.gridGPreferences.attach( labelSpaceGPreferences5, 5, 13, 1, 1 );
        let labelSpaceGPreferences6 = new Gtk.Label( { label: "\t", xalign: 0 } );
        this.gridGPreferences.attach( labelSpaceGPreferences6, 6, 13, 1, 1 );


        // Behavior tab
        this.gridBPreferences = new Gtk.Grid();
        this.gridBPreferences.set_column_homogeneous( false );
        this.gridBPreferences.margin = this.gridBPreferences.row_spacing = 10;
        this.gridBPreferences.column_spacing = 2;

        this.scrollWindowBPreferences = new Gtk.ScrolledWindow(
            {
                //'child': this.gridBPreferences,
                'hscrollbar-policy': Gtk.PolicyType.AUTOMATIC,
                'vscrollbar-policy': Gtk.PolicyType.AUTOMATIC,
                'hexpand': true, 'vexpand': true
            }
        );

        if( USE_GTK4 )
            this.scrollWindowBPreferences.set_child( this.gridBPreferences );
        else {
            this.scrollWindowBPreferences.add( this.gridBPreferences );     // add_with_viewport has been deprecated since 3.8 (https://developer.gnome.org/gtk3/stable/GtkScrolledWindow.html#gtk-scrolled-window-add-with-viewport)
            this.scrollWindowBPreferences.show_all();
        }

        this.labelBehavior = new Gtk.Label( { label: _( "Behavior" ) } );    // Tab
        this.notebook.append_page( this.scrollWindowBPreferences, this.labelBehavior );


        // Auto-hide settings
        this.labelAutohide = new Gtk.Label( { label: _( "Auto-hide panel " ), xalign: 0 } );    // Menu Item    switch
        this.valueAutohideBox = new Gtk.Box( { hexpand: false } );   // Place switch in Gtk.Box with no hexpand, allows it to align left.
        this.valueAutohide = new Gtk.Switch( { active: this.settings.get_boolean( "autohide-panel" ) } );
        this.valueAutohide.connect( 'notify::active', lang.bind( this, this.changeAutohide ) );

        if( USE_GTK4 ) {
            this.valueAutohideBox.prepend( this.labelAutohide );
            this.valueAutohideBox.append( this.valueAutohide );
        }
        else {
            this.valueAutohideBox.add( this.labelAutohide );
            this.valueAutohideBox.add( this.valueAutohide );
        }

        this.gridBPreferences.attach( this.valueAutohideBox, 1, 1, 1, 1 );


        // Associated menu item - pressure threshold
        this.labelPressure = new Gtk.Label( { label: _( "Pressure " ), xalign: 1 } );     // Menu Item    drop-down
        this.gridBPreferences.attach( this.labelPressure, 3, 1, 1, 1 );
        this.valuePressure = new Gtk.SpinButton( { halign: Gtk.Align.START, margin_top: 0 } );
        this.valuePressure.set_sensitive( true );
        this.valuePressure.set_range( 10, 500 );
        this.valuePressure.set_value( this.settings.get_double( "autohide-pressure-threshold") * 1 );
        this.valuePressure.set_increments( 10, 20 );
        this.valuePressure.connect(
            'value-changed',
            lang.bind(
                this,
                ( b ) => {
                    let s = b.get_value_as_int() / 1;
                    this.settings.set_double( "autohide-pressure-threshold", s );
                }
            )
        );
        this.gridBPreferences.attach( this.valuePressure, 4, 1, 1, 1 );


        // Associated menu item - auto-hide delay
        this.labelAutohideDelay = new Gtk.Label( { label: _( "Auto-hide delay " ), xalign: 1 } );     // Menu Item    drop-down
        this.gridBPreferences.attach( this.labelAutohideDelay, 3, 2, 1, 1 );
        this.valueAutohideDelay = new Gtk.SpinButton( { halign: Gtk.Align.START, margin_top: 0 } );
        this.valueAutohideDelay.set_sensitive( true );
        this.valueAutohideDelay.set_range( 0, 10 );
        this.valueAutohideDelay.set_value( this.settings.get_double( "autohide-delay" ) * 1 );
        this.valueAutohideDelay.set_increments( 1, 2 );
        this.valueAutohideDelay.connect(
            'value-changed',
            lang.bind(
                this,
                ( b ) => {
                    let d = b.get_value_as_int() / 1;
                    this.settings.set_double( "autohide-delay", d );
                }
            )
        );
        this.gridBPreferences.attach( this.valueAutohideDelay, 4, 2, 1, 1 );


        // Associated menu item - auto-hide animation time
        this.labelAutohideAnimationTime = new Gtk.Label( { label: _( "Animation time " ), xalign: 1 } );     // Menu Item    drop-down
        this.gridBPreferences.attach( this.labelAutohideAnimationTime, 3, 3, 1, 1 );
        this.valueAutohideAnimationTime = new Gtk.SpinButton( { halign: Gtk.Align.START, margin_top: 0 } );
        this.valueAutohideAnimationTime.set_sensitive( true );
        this.valueAutohideAnimationTime.set_range( 0, 3 );
        this.valueAutohideAnimationTime.set_value( this.settings.get_double( "autohide-animation-time" ) * 1 );
        this.valueAutohideAnimationTime.set_increments( 1, 2 );
        this.valueAutohideAnimationTime.connect(
            'value-changed',
            lang.bind(
                this,
                ( b ) => {
                    let t = b.get_value_as_int() / 1;
                    this.settings.set_double( "autohide-animation-time", t );
                }
            )
        );
        this.gridBPreferences.attach( this.valueAutohideAnimationTime, 4, 3, 1, 1 );


        // Associated menu item - auto-hide animation delay
        this.labelAutohideAnimationDelay = new Gtk.Label( { label: _( "Animation delay " ), xalign: 1 } );     // Menu Item    drop-down
        this.gridBPreferences.attach( this.labelAutohideAnimationDelay, 3, 4, 1, 1 );
        this.valueAutohideAnimationDelay = new Gtk.SpinButton( { halign: Gtk.Align.START, margin_top: 0 } );
        this.valueAutohideAnimationDelay.set_sensitive( true );
        this.valueAutohideAnimationDelay.set_range( 0, 5 );
        this.valueAutohideAnimationDelay.set_value( this.settings.get_double( "autohide-animation-delay" ) * 1 );
        this.valueAutohideAnimationDelay.set_increments( 1, 2 );
        this.valueAutohideAnimationDelay.connect(
            'value-changed',
            lang.bind(
                this,
                ( b ) => {
                    let d = b.get_value_as_int() / 1;
                    this.settings.set_double( "autohide-animation-delay", d );
                }
            )
        );
        this.gridBPreferences.attach( this.valueAutohideAnimationDelay, 4, 4, 1, 1 );


        // Add a little space between setting groups so that we don't confuse the end user
        this.hcssep = new Gtk.Box( { hexpand: false } );

        if( USE_GTK4 )
            this.hcssep.append( new Gtk.Label( { label: "\t" } ) );
        else
            this.hcssep.add( new Gtk.Label( { label: "\t" } ) );

        this.gridBPreferences.attach( this.hcssep, 1, 5, 1, 1 );


        // Enabled hot corner settings
        this.labelDisableHotCorner = new Gtk.Label( { label: _( "Hot corner disabled" ), xalign: 0 } );    // Menu Item
        this.gridBPreferences.attach( this.labelDisableHotCorner, 1, 6, 1, 1 );


        // Is the switch for the enable hot corner settings
        this.valueDisableHotCornerBox = new Gtk.Box( { hexpand: false } );
        this.valueDisableHotCorner = new Gtk.Switch( { active: this.settings.get_boolean( "hot-corner-disabled" ) } );
        this.valueDisableHotCorner.connect( 'notify::active', this.changeDisableHotCorner.bind( this ) );

        if( USE_GTK4 )
            this.valueDisableHotCornerBox.append( this.valueDisableHotCorner );
        else
            this.valueDisableHotCornerBox.add( this.valueDisableHotCorner );

        this.gridBPreferences.attach( this.valueDisableHotCornerBox, 4, 6, 1, 1 );


        // Tab footer / page number for the behavior tab
        let labelSpaceBPreferences0 = new Gtk.Label( { label: "\t", xalign: 0 } );
        this.gridBPreferences.attach( labelSpaceBPreferences0, 0, 7, 1, 1 );
        let labelSpaceBPreferences2 = new Gtk.Label( { label: "\t", xalign: 0, hexpand: true } );
        this.gridBPreferences.attach( labelSpaceBPreferences2, 2, 7, 1, 1 );
        let labelSpaceBPreferences3 = new Gtk.Label( { label: "\t", xalign: 0, hexpand: false } );
        this.gridBPreferences.attach( labelSpaceBPreferences3, 3, 7, 1, 1 );
        let labelSpaceBPreferences5 = new Gtk.Label( { label: "2/3", xalign: 1 } );
        this.gridBPreferences.attach( labelSpaceBPreferences5, 5, 7, 1, 1 );
        let labelSpaceBPreferences6 = new Gtk.Label( { label: "\t", xalign: 0 } );
        this.gridBPreferences.attach( labelSpaceBPreferences6, 6, 7, 1, 1 );


        this.gridAbout = new Gtk.Grid();     // New tab
        this.gridAbout.set_column_homogeneous( false );
        this.gridAbout.margin = this.gridAbout.row_spacing = 10;
        this.gridAbout.column_spacing = 2;
        this.scrollWindowAbout = new Gtk.ScrolledWindow(
            {
                //'child': this.gridAbout,
                'hscrollbar-policy': Gtk.PolicyType.AUTOMATIC,
                'vscrollbar-policy': Gtk.PolicyType.AUTOMATIC,
                'hexpand': true, 'vexpand': true
            }
        );

        if( USE_GTK4 )
            this.scrollWindowAbout.set_child( this.gridAbout );
        else {
            this.scrollWindowAbout.add( this.gridAbout );     // add_with_viewport has been deprecated since 3.8 (https://developer.gnome.org/gtk3/stable/GtkScrolledWindow.html#gtk-scrolled-window-add-with-viewport)
            this.scrollWindowAbout.show_all();
        }

        this.labelAbout = new Gtk.Label( { label: _( "About" ) } );    // Tab
        this.notebook.append_page( this.scrollWindowAbout, this.labelAbout );


        //https://mail.google.com/mail/?extsrc=mailto&url=mailto:support@mmogp.com?subject=Feedback/Suggestions%20for%20MMOD%20Gnome
        //https://mail.google.com/mail/?extsrc=mailto&url=mailto:support@mmogp.com?subject=Issue/Bug%20for%20MMOD%20Gnome:%20
        this.linkMMODHomeImage = new Gtk.Image( { file: ICON_LOGO } );
        this.linkMMODPanelRepoImage = new Gtk.Image( { file: ICON_M4IKEN } );
        this.linkMMODPanelIssueImage = new Gtk.Image( { file: ICON_M4IKEN_RED } );
        this.linkGLHomeImage = new Gtk.Image( { file: ICON_GLOOK } );
        this.linkDonateImage = new Gtk.Image( { file: ICON_DONATE } );
        this.linkMMODGFeedbackImage = new Gtk.Image( { file: ICON_GMAIL } );
        this.linkMMODEFeedbackImage = new Gtk.Image( { file: ICON_EMAIL } );
        this.linkGnomeImage = new Gtk.Image( { file: ICON_GNOME } );


        this.labelMMODHomeLink = new Gtk.LinkButton(
            (
                ( USE_GTK4 ) ?
                    {
                        child: this.linkMMODHomeImage,
                        //label: " .",
                        uri: "http://www.mmod.co",
                        //xalign: 0,
                        hexpand: false
                    } :
                    {
                        image: this.linkMMODHomeImage,
                        //label: " .",
                        uri: "http://www.mmod.co",
                        xalign: 0,
                        hexpand: false
                    }
            )
        );

        if( sver[1] !== 4 )
            if( !USE_GTK4 )
                this.labelMMODHomeLink.set_always_show_image( true );

        this.gridAbout.attach( this.labelMMODHomeLink, 0, 0, 1, 1 );


        this.labelMMODPanelVersion = new Gtk.Label( { label: _( "MMOD-Panel v" ) + mver, xalign: 1, hexpand: false } );
        this.gridAbout.attach( this.labelMMODPanelVersion, 3, 0, 1, 1 );

        let labelSpaceGPreferencesXX = new Gtk.Label( { label: "\t\t\t\t", xalign: 1 } );
        this.gridAbout.attach( labelSpaceGPreferencesXX, 2, 1, 1, 1 );

        this.labelMMODPanelRepoLink = new Gtk.LinkButton(
            (
                ( USE_GTK4 ) ?
                    {
                        child: this.linkMMODPanelRepoImage,
                        label: " MMOD Git Repository",
                        uri: "https://gitlab.com/mmod/mmod-panel",
                        //xalign: 0,
                        hexpand: false
                    } :
                    {
                        image: this.linkMMODPanelRepoImage,
                        label: " MMOD Git Repository",
                        uri: "https://gitlab.com/mmod/mmod-panel",
                        xalign: 0,
                        hexpand: false
                    }
            )
        );

        if( sver[1] !== 4 )
            if( !USE_GTK4 )
                this.labelMMODPanelRepoLink.set_always_show_image( true );

        this.gridAbout.attach( this.labelMMODPanelRepoLink, 3, 1, 1, 1 );


        this.labelGLHomeLink = new Gtk.LinkButton(
            (
                ( USE_GTK4 ) ?
                    {
                        child: this.linkGLHomeImage,
                        label: " gnome-look.org",
                        uri: "https://extensions.gnome.org/extension/898/mmod-panel",
                        //xalign: 0,
                        hexpand: false
                    } :
                    {
                        image: this.linkGLHomeImage,
                        label: " gnome-look.org",
                        uri: "https://extensions.gnome.org/extension/898/mmod-panel",
                        xalign: 0,
                        hexpand: false
                    }
            )
        );

        if( sver[1] !== 4 )
            if( !USE_GTK4 )
                this.labelGLHomeLink.set_always_show_image( true );

        this.gridAbout.attach( this.labelGLHomeLink, 3, 2, 1, 1 );


        this.labelMMODPanelIssueLink = new Gtk.LinkButton
        (
            (
                ( USE_GTK4 ) ?
                    {
                        child: this.linkMMODPanelIssueImage,
                        label: " Report an issue",
                        uri: "https://gitlab.com/mmod/mmod-panel/issues",
                        //xalign: 0,
                        hexpand: false
                    } :
                    {
                        image: this.linkMMODPanelIssueImage,
                        label: " Report an issue",
                        uri: "https://gitlab.com/mmod/mmod-panel/issues",
                        xalign: 0,
                        hexpand: false
                    }
            )
        );

        if( sver[1] !== 4 )
            if( !USE_GTK4 )
                this.labelMMODPanelIssueLink.set_always_show_image( true );

        this.gridAbout.attach( this.labelMMODPanelIssueLink, 3, 3, 1, 1 );


        this.labelDonateLink = new Gtk.LinkButton(
            (
                ( USE_GTK4 ) ?
                    {
                        child: this.linkDonateImage,
                        //label: " Donate",
                        uri: "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=H6QJC5JS7PEAC",
                        //xalign: 0,
                        hexpand: false
                    } :
                    {
                        image: this.linkDonateImage,
                        //label: " Donate",
                        uri: "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=H6QJC5JS7PEAC",
                        xalign: 0,
                        hexpand: false
                    }
            )
        );

        if( sver[1] !== 4 )
            if( !USE_GTK4 )
                this.labelDonateLink.set_always_show_image( true );

        this.gridAbout.attach( this.labelDonateLink, 0, 4, 1, 1 );


        this.emailMeBox = new Gtk.Box( { hexpand: true } );
        this.labelGmailMeLink = new Gtk.LinkButton(
            (
                ( USE_GTK4 ) ?
                    {
                        child: this.linkMMODGFeedbackImage,
                        label: " Gmail me",
                        uri: "https://mail.google.com/mail/?extsrc=mailto&url=mailto:support@mmod.co?subject=Feedback/Suggestions%20for%20MMOD%20Panel",
                        //xalign: 0,
                        hexpand: false
                    } :
                    {
                        image: this.linkMMODGFeedbackImage,
                        label: " Gmail me",
                        uri: "https://mail.google.com/mail/?extsrc=mailto&url=mailto:support@mmod.co?subject=Feedback/Suggestions%20for%20MMOD%20Panel",
                        xalign: 0,
                        hexpand: false
                    }
            )
        );

        if( sver[1] !== 4 )
            if( !USE_GTK4 )
                this.labelGmailMeLink.set_always_show_image( true );

        //this.gridAbout.attach( this.labelGmailMeLink, 3, 6, 1, 1 );


        this.labelEmailMeLink = new Gtk.LinkButton(
            (
                ( USE_GTK4 ) ?
                    {
                        child: this.linkMMODEFeedbackImage,
                        label: " Email me",
                        uri: "mailto:support@mmod.co?subject=Feedback/Suggestions%20for%20MMOD%20Panel&Body=mmod-panel%20v1.1.1-8:\n\n",
                        //xalign: 0,
                        hexpand: false
                    } :
                    {
                        image: this.linkMMODEFeedbackImage,
                        label: " Email me",
                        uri: "mailto:support@mmod.co?subject=Feedback/Suggestions%20for%20MMOD%20Panel&Body=mmod-panel%20v1.1.1-8:\n\n",
                        xalign: 0,
                        hexpand: false
                    }
            )
        );

        if( sver[1] !== 4 )
            if( !USE_GTK4 )
                this.labelEmailMeLink.set_always_show_image( true );

        if( USE_GTK4 ) {
            this.emailMeBox.append( this.labelGmailMeLink );
            this.emailMeBox.append( this.labelEmailMeLink );
        }
        else {
            this.emailMeBox.add( this.labelGmailMeLink );
            this.emailMeBox.add( this.labelEmailMeLink );
        }

        this.gridAbout.attach( this.emailMeBox, 3, 4, 1, 1 );
        //this.gridAbout.attach( this.labelEmailMeLink, 4, 6, 1, 1 );

        //let labelSpaceAbout = new Gtk.Label( { label: "2/2", xalign: 1, hexpand: true } );
        //this.gridAbout.attach( labelSpaceAbout, 3, 8, 1, 1 );
        //let labelSpaceAbout1 = new Gtk.Label( { label: "\t", xalign: 0 } );
        //this.gridAbout.attach( labelSpaceAbout1, 0, 8, 1, 1 );
        //let labelSpaceAbout2 = new Gtk.Label( { label: "\t", xalign: 0, hexpand: false } );
        //this.gridAbout.attach( labelSpaceAbout2, 3, 8, 1, 1 );
        let labelSpaceAbout = new Gtk.Label( { label: "3/3", xalign: 1, hexpand: false } );
        this.gridAbout.attach( labelSpaceAbout, 3, 8, 1, 1 );



        if( !USE_GTK4 )
            this.notebook.show_all();

        return this.notebook;
    },

    changePanelSetting: ( object ) => {
        this.settings.set_boolean( 'mmod-panel-enabled', object.active );
    },

    changePanelPosition: ( object ) => {
        this.settings.set_enum( 'panel-position', object.active );
    },

    changePanelButton: ( object, pspec ) => {
        this.settings.set_boolean( 'panel-button-enabled', object.active );
    },

    changePanelButtonIcon: ( object, pspec ) => {
        // Get comfort level so we can set appropriate icon size in the path
        let comfortLevel = this.settings.get_enum( 'comfort-level' ), iconSize;
        switch( comfortLevel ) {
            case 0:
                iconSize = 24;
            break;

            case 2:
                iconSize = 48;
            break;

            default:
                iconSize = 32;
            break;
        }

        // Now set the panel button icon path
        let old = this.settings.get_enum( 'panel-button-icon' ), inew = this.valuePanelButtonIcon.get_active();
        switch( inew ) {
            case 0: // M4Star
                this.valueCustomPanelButtonIcon.set_from_file( our.path + '/res/img/mico/mstar/mmod-logo-fin-' + iconSize + '.png' );
                this.dipath = our.path + '/res/img/mico/mstar/mmod-logo-fin-' + iconSize + '.png';
                this.settings.set_string( 'panel-button-icon-path', this.dipath );
                break;

            case 1: // Apps
                this.valueCustomPanelButtonIcon.set_from_file( our.path + '/res/img/mico/mapp/mapp-' + iconSize + '.png' );
                this.dipath = our.path + '/res/img/mico/mapp/mapp-' + iconSize + '.png';
                this.settings.set_string( 'panel-button-icon-path', this.dipath );
                break;

            case 2: // Debian
                this.valueCustomPanelButtonIcon.set_from_file( our.path + '/res/img/debico/drnd/debico_' + iconSize + '.png' );
                this.dipath = our.path + '/res/img/debico/drnd/debico_' + iconSize + '.png';
                this.settings.set_string( 'panel-button-icon-path', this.dipath );
                break;

            case 3: // Fedora
                this.valueCustomPanelButtonIcon.set_from_file( our.path + '/res/img/fedico/fgrey/fedora-' + iconSize + '.png' );
                this.dipath = our.path + '/res/img/fedico/fgrey/fedora-' + iconSize + '.png';
                this.settings.set_string( 'panel-button-icon-path', this.dipath );
                break;
        }

        if( old === 4 && inew !== 4 )
            this.valuePanelButtonIcon.remove( 4 );

        this.settings.set_enum( 'panel-button-icon', inew );
    },

    changeCustomPanelButtonIcon: ( object, pspec ) => {
        // Get the current path to the icon
        let ipath = this.settings.get_string( 'panel-button-icon-path' );

        // Build a file picker for selecting the icon
        this.ipicker = new Gtk.FileChooserDialog(
            {
                title: _( 'MMOD Panel - Panel Button Icon' ),
                action: Gtk.FileChooserAction.OPEN
            }
        );
        this.ipicker.add_button( Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL );
        this.ipicker.add_button( Gtk.STOCK_OPEN, Gtk.ResponseType.ACCEPT );
        this.ipicker.add_button( 'Reset', Gtk.ResponseType.NONE );
        this.ipicker.set_filename( ipath );

        // Add an image preview
        this.ipreview = new Gtk.Image();
        this.ipreview.set_from_file( our.path + '/res/img/appico/appview-button-default.svg' );
        this.ipicker.set_preview_widget( this.ipreview );
        //this.ipicker.set_use_preview_label( false );
        if( ipath !== this.dipath )
            this.dipath = ipath;

        this.loadCustomPanelButtonIconPreview();
        this.updatepreview = this.ipicker.connect( 'update-preview', lang.bind( this, this.loadCustomPanelButtonIconPreview ) );

        // Add image file filters
        let filter = new Gtk.FileFilter();
        filter.set_name( _( 'Images' ) );
        filter.add_pattern( '*.png' );
        filter.add_pattern( '*.jpg' );
        filter.add_pattern( '*.gif' );
        filter.add_pattern( '*.svg' );
        filter.add_pattern( '*.ico' );
        this.ipicker.add_filter( filter );

        // Load file picker, await response from user
        let response = this.ipicker.run();

        switch( response ) {
            case -3:    // open
                this.customPanelButtonIconFilename = this.ipicker.get_filename();
                if( this.customPanelButtonIconFilename !== this.dipath ) {
                    this.dipath = this.customPanelButtonIconFilename;
                    this.settings.set_string( 'panel-button-icon-path', this.dipath );
                    this.loadCustomPanelButtonIcon();
                }
                break;

            case -1:    // reset
                this.customPanelButtonIconFilename = our.path + '/res/img/mico/mstar/mmod-logo-fin-48.png';
                this.dipath = this.customPanelButtonIconFilename;
                this.settings.set_string( 'panel-button-icon-path', this.dipath );
                this.loadCustomPanelButtonIcon();
                break;
        }

        this.ipicker.disconnect( this.updatepreview );
        this.ipicker.destroy();

        this.valuePanelButtonIcon.append_text( _( "Custom" ) );
        this.valuePanelButtonIcon.set_active( 4 );

        this.settings.set_enum( 'panel-button-icon', 4 );
    },

    loadCustomPanelButtonIconPreview: () => {
        let preview = false;

        this.ipfilename = this.ipicker.get_preview_filename();

        if( !this.ipfilename )
            if( this.dipath )
                this.ipfilename = this.dipath;

        try {
            this.ipreview.clear();
            this.ipreview.set_from_file( this.ipfilename );
            preview = true;
        }
        catch( e ) {
            log( 'Error: ' + e.what() + ' [prefs.js `loadCustomPanelButtonIconPreview`' );
            preview = false;
        }

        this.ipicker.set_preview_widget_active( preview );
    },

    loadCustomPanelButtonIcon: () => {
        this.valueCustomPanelButtonIcon.clear();
        if( this.dipath )
            this.valueCustomPanelButtonIcon.set_from_file( this.dipath );
        else
            this.valueCustomPanelButtonIcon.set_from_file( our.path + '/res/img/mico/mstar/mmod-logo-fin-48.png' );
    },

    changeDateInTray: ( object ) => {
        this.settings.set_boolean( 'date-in-sys-tray', object.active );
    },

    changeDisplayFavorites: ( object ) => {
        this.settings.set_boolean( 'display-favorites-enabled', object.active );
    },

    changeShowRunningApps: ( object ) => {
        this.settings.set_boolean( 'show-running-apps', object.active );
    },

    changeShowFavsBeforePrefs: ( object ) => {
        this.settings.set_boolean( 'favorites-before-preferences', object.active );
    },

    changeShowInTrayMenu: ( object ) => {
        this.settings.set_boolean( 'show-in-tray-menu', object.active );
    },

    changeComfortLevel: ( object ) => {
        this.settings.set_enum( 'comfort-level', object.active );
    },

    changeAutohide: ( object ) => {
        this.settings.set_boolean( 'autohide-panel', object.active );
    },

    changeDisableHotCorner: ( object ) => {
        this.settings.set_boolean( 'hot-corner-disabled', object.active );
    },

    onHoverEvent: ( object ) => {
        this.hoverComponent = this.settings.get_enum( 'placement-elements-selection' );
        this.settings.set_int( 'hover-event', this.hoverComponent + 1 );
    }
}
