/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// DEFINES
const {
    config,
    extensionUtils
}                                   = imports.misc;

const   GNOME_SHELL_VERSION_PARTS   = config.PACKAGE_VERSION.split( '.' ),
        SHELL_VERSION_MAJOR         = Number.parseInt( GNOME_SHELL_VERSION_PARTS[0] ),
        SHELL_VERSION_MINOR         = Number.parseInt( GNOME_SHELL_VERSION_PARTS[1] ),
        USE_GTK4                    = !( SHELL_VERSION_MAJOR < 40 ),
        USE_CONVENIENCE             = !USE_GTK4 && SHELL_VERSION_MINOR < 36,
        USE_ESYS                    = !USE_GTK4 && SHELL_VERSION_MINOR <= 2;

const   our                         = extensionUtils.getCurrentExtension();
var     MMODPanelLibrary            = our.imports.lib.mmod;
//const   Steward                     = our.imports.lib.mmod.settings.Steward;
//const   Rigging                     = our.imports.lib.mmod.settings.Rigging;

const { main }                      = imports.ui;

// With out this logging - the system literally does not freaking load
// the imports, and claims it cannot find them!?!?!?
log( MMODPanelLibrary );
log( MMODPanelLibrary.settings );
log( MMODPanelLibrary.settings.Rigging );
log( MMODPanelLibrary.settings.Rigging.Rigging );
log( MMODPanelLibrary.settings.Steward );
log( MMODPanelLibrary.settings.Steward.Steward );
log( MMODPanelLibrary.position );
log( MMODPanelLibrary.position.Modification );
log( MMODPanelLibrary.behavior );
log( MMODPanelLibrary.behavior.Modification );
log( MMODPanelLibrary.activities );
log( MMODPanelLibrary.activities.Modification );
log( MMODPanelLibrary.favorites );
log( MMODPanelLibrary.favorites.Modification );
log( MMODPanelLibrary.aggregate );
log( MMODPanelLibrary.aggregate.Modification );
log( MMODPanelLibrary.date );
log( MMODPanelLibrary.date.Modification );
//

// Extension Class
class MMODPanelGnomeExtension {
    constructor() {
        this._connections = [];
        this._monitorsChangedId = null;
        this._rigging = null;
        this._steward = null;
    }

    enable() {
        this._rigging = new MMODPanelLibrary.settings.Rigging( GNOME_SHELL_VERSION_PARTS );
        this._steward = new MMODPanelLibrary.settings.Steward( { rigging: this._rigging } );

        this._steward.modify();
        this.connect();
    }

    disable() {
        this.disconnect();
        this._steward.unmodify();
    }

    connect() {
        this._connections = [
             this._rigging.settings.connect( 'changed::mmod-panel-enabled', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::comfort-level', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::panel-position', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::show-in-tray-menu', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::panel-button-enabled', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::panel-button-icon', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::panel-button-icon-path', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::display-favorites-enabled', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::show-running-apps', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::favorites-before-preferences', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::date-in-sys-tray', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::autohide-panel', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::autohide-pressure-threshold', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::autohide-delay', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::autohide-animation-time', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::autohide-animation-delay', this.onPreferenceChanged.bind( this ) ),
             this._rigging.settings.connect( 'changed::hot-corner-disabled', this.onPreferenceChanged.bind( this ) )
        ];

        // Let's handle some global edge cases::
        this._monitorsChangedId = main.layoutManager.connect( 'monitors-changed', this.onPreferenceChanged.bind( this ) );
    }

    disconnect() {
        if( this._connections ) {
            this._connections.forEach(
                connectionId => this._rigging.settings.disconnect( connectionId ),
                this
            );

            this._connections = null;
        }

        // Don't forget to release connections made for our global edge cases:
        if( this._monitorsChangedId ) {
            main.layoutManager.disconnect( this._monitorsChangedId );
            this._monitorsChangedId = null;
        }
    }


    onPreferenceChanged() {
        this.disable();
        this.enable();
    }


    loadPreferences() {
        extensionUtils.openPrefs();
    }
}


/**
 * Main entry point to the application
 *
 * @returns { mmodge }
 */
function init() {
    return new MMODPanelGnomeExtension();
    //return new mmodge();
}
