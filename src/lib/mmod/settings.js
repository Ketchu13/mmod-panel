/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// Deps
const {
        config,
        extensionUtils
}                                   = imports.misc;

const   GNOME_SHELL_VERSION_PARTS   = config.PACKAGE_VERSION.split( '.' ),
        SHELL_VERSION_MAJOR         = Number.parseInt( GNOME_SHELL_VERSION_PARTS[0] ),
        SHELL_VERSION_MINOR         = Number.parseInt( GNOME_SHELL_VERSION_PARTS[1] ),
        USE_GTK4                    = !( SHELL_VERSION_MAJOR < 40 ),
        USE_CONVENIENCE             = !USE_GTK4 && SHELL_VERSION_MINOR < 36,
        USE_ESYS                    = !USE_GTK4 && SHELL_VERSION_MINOR <= 2;

const   our                         = extensionUtils.getCurrentExtension();
var     MMODPanelLibrary            = our.imports.lib.mmod;


class Rigging {
    constructor( shellVersion ) {
        // These don't have side-effets, so they are marked private
        this._version   = shellVersion;
        this._path      = our.path;
        this._schema    = "org.gnome.shell.extensions.mmod-panel";

        // This has side effects, so do not mark it as private
        this.settings   = extensionUtils.getSettings( this._schema );
    }

}


class Steward {
    constructor( configuration ) {
        // The rigging itself, and all other properties here are private
        // and have no side-effects
        this._rigging = ( configuration.rigging ) ? configuration.rigging : null;

        this._comfortLevel = null;
        this._comfortSettings = {
            position: null,
            panelButton: null,
            favoritesDisplay: null
        };
        this._customizations = this.init();
    }

    init() {
        this._comfortSettings = this.loadComfortSettings();

        return {
            topBar: {
                position: new MMODPanelLibrary.position.Modification( { rigging: this._rigging, comfortSettings: this._comfortSettings.position } ),
                behavior: new MMODPanelLibrary.behavior.Modification( { rigging: this._rigging } ),
                panelButton: new MMODPanelLibrary.activities.Modification( { rigging: this._rigging, comfortSettings: this._comfortSettings.panelButton } ),
                favoritesDisplay: new MMODPanelLibrary.favorites.Modification( { rigging: this._rigging, comfortSettings: this._comfortSettings.favorites } ),
                aggregateMenu: new MMODPanelLibrary.aggregate.Modification( { rigging: this._rigging } ),
                systemDateTimeMenu: new MMODPanelLibrary.date.Modification( { rigging: this._rigging } )
            }
        };
    }

    modify() {
        if( this._rigging.settings.get_boolean( 'mmod-panel-enabled' ) ) {
            // Initialize any modifications to:
            this._customizations.topBar.position.enable();
            this._customizations.topBar.behavior.enable();
            this._customizations.topBar.panelButton.enable();
            this._customizations.topBar.favoritesDisplay.enable();
            this._customizations.topBar.aggregateMenu.enable();
            this._customizations.topBar.systemDateTimeMenu.enable();
        }
    }

    unmodify() {
        // Destroy any modifications to:
        this._customizations.topBar.position.disable();
        this._customizations.topBar.behavior.disable();
        this._customizations.topBar.panelButton.disable();
        this._customizations.topBar.favoritesDisplay.disable();
        this._customizations.topBar.aggregateMenu.disable();
        this._customizations.topBar.systemDateTimeMenu.disable();

        // Reset comfort level settings
        this._comfortLevel = null;
        //this.comfortSettings = null;

        ( Object.entries( this._customizations.topBar ) ).forEach(
            ( [ key/*, modification*/ ] ) => {
                delete this._customizations.topBar[key];
                this._customizations.topBar[key] = null;
            }
        );

        /*for( let modification in Object.entries( this._customizations.topBar ) )
        {
            delete modification[1];
            this._customizations.topBar[modification[0]] = null;
        }*/
    }

    loadComfortSettings() {
        this._comfortLevel = this._rigging.settings.get_enum( 'comfort-level' );

        let returnable = null;
        switch( this._comfortLevel ) {
            case 0:
                // Compact ( 24/16)
                returnable = {
                    position: {
                        containerStyle: 'mmod-panel-style-compact',
                        containerHeight: 28
                    },
                    panelButton: { buttonStyle: 'mmod-panel-button-icon-style-compact' },
                    favorites: { favoriteSize: 16 }
                };
                break;

            case 2:
                // Comfortable ( 48/36 )
                returnable = {
                    position: {
                        containerStyle: 'mmod-panel-style-comfortable',
                        containerHeight: 52
                    },
                    panelButton: { buttonStyle: 'mmod-panel-button-icon-style-comfortable' },
                    favorites: { favoriteSize: 44 }
                };
                break;

            default:
                // Cozy (36/22)
                returnable = {
                    position: {
                        containerStyle: 'mmod-panel-style-cozy',
                        containerHeight: 36
                    },
                    panelButton: { buttonStyle: 'mmod-panel-button-icon-style-cozy' },
                    favorites: { favoriteSize: 22 }
                };
                break;
        }

        // Because we keep everything instantiated even if disabled, we must update comfort settings manually
        return returnable;
    }
}
