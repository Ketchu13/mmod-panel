/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// DEFINES
const { main } = imports.ui;


class Modification {
    constructor( configuration ) {
        this._rigging   = configuration.rigging;

        this._panelBox  = null;
        this._menu      = main.panel.statusArea.dateMenu;
        this._centerBox = main.panel._centerBox;
        this._rightBox  = main.panel._rightBox;

        this._realize   = false;

        this.active     = false;
    }

    enable() {
        if( this._rigging.settings.get_boolean( 'date-in-sys-tray' ) ) {
            this._panelBox = main.panel.actor.get_parent();

            // Handle realize if applicable
            if( !this._panelBox && !this._realize ) {
                // Shell/Extension has just initialized
                this._realize = this._panelBox.connect( 'realize', this.enable.bind( this ) );

                // Do not allow the method to continue
                return;
            }

            if( this._panelBox && this._realize ) {
                // We're initializing after a disable/enable combo-invocation
                this._panelBox.disconnect( this._realize );
                this._realize = false;
            }

            // This is obsolete (reparent() was seemingly removed):
            //this.menu.actor.reparent( this.rbox );

            // Reimplementation:
            //
            // First we need to remove our menu actor from it's parent, so get the parent:
            let parent = this._menu.actor.get_parent();

            // If the parent is found, remove the menu child from it:
            if( parent ) {
                parent.remove_child( this._menu );

                // Finally, add our menu as a child to the rbox:
                this._rightBox.add_child( this._menu.actor );

                // And note that our menu is in the panel's tray area:
                this._menu.inTray = true;
            }
        }
        else
            this._menu.inTray = false;

        this.active = true;
    }

    disable() {
        if( this.active ) {
            if( this._menu.inTray ) {
                // This is obsolete (reparent() was seemingly removed):
                //this.menu.actor.reparent( this.cbox );

                // Reimplimentation:
                //
                // We need to remove the menu from its parent (the tray):
                let parent = this._menu.actor.get_parent();

                // If the parent is found, remove the menu child from it:
                if( parent ) {
                    parent.remove_child( this._menu );

                    // Finally, add our menu as a child to the cbox:
                    this._centerBox.add_child( this._menu.actor );

                    // Denote that the menu is no longer in the tray:
                    this._menu.inTray = null;
                }
            }

            delete this._menu.inTray;

            this.active = false;
        }
    }
}
