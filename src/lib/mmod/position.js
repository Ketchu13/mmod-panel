/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// DEFINES
const {
        main
}                       = imports.ui;
const { config }        = imports.misc;

const   GNOME_SHELL_VERSION_PARTS   = config.PACKAGE_VERSION.split( '.' ),
        SHELL_VERSION_MAJOR         = Number.parseInt( GNOME_SHELL_VERSION_PARTS[0] );

const   EDGE_BOTTOM     = 0,
        EDGE_TOP        = 1;


class Modification {
    constructor( configuration ) {
        this._rigging = configuration.rigging;
        this._comfortSettings = configuration.comfortSettings;


        this._panel = main.panel;
        this._taskbar = null;
        this._monitor = main.layoutManager.primaryMonitor;

        this._realize = null;
        this.active = false;
    }

    enable() {
        this._taskbar = this._panel.actor.get_parent();

        // Handle realize if applicable
        if( !this._taskbar && !this._realize ) {
            // Shell/Extension has just initialized
            this._realize = this._taskbar.connect( 'realize', this.enable.bind( this ) );

            // Do not allow the method to continue
            return;
        }

        if( this._taskbar && this._realize ) {
            // This is the second invocation, called after realization of the panelBox
            this._taskbar.disconnect( this._realize );
            this._realize = null;
        }

        // Apply style modifications first so that we get proper the height calculated for bottom positioning
        ( SHELL_VERSION_MAJOR >= 42 ) ?
            this._panel.add_style_class_name( this._comfortSettings.containerStyle ) :
            this._panel._addStyleClassName( this._comfortSettings.containerStyle );

        // Handle the panel location customization:
        this._panel.edge = this._rigging.settings.get_enum( 'panel-position' );

        // EDGE_BOTTOM:
        if( !this._panel.edge ) {
            // Try to dynamically handle positioning based on height:
            let height = this._taskbar.get_height();
            if( !height ) {
                // Fall back to hard-coded heights if necessary:
                switch( this._comfortSettings.containerStyle ) {
                    case 'mmod-panel-style-compact':
                        height = 28;
                        break;
                    case 'mmod-panel-style-comfortable':
                        height = 52;
                        break;
                    default:    // 'mmod-panel-style-cozy':
                        height = 36;
                }
            }

            this._taskbar.set_y( this._monitor.height - height );
        }
        else    // EDGE_TOP:
            this._taskbar.set_y( this._monitor.y );

        this.active = true;
    }

    disable() {
        if( this.active ) {
            // Replace the panelBox to its rightful position
            this._taskbar.set_y( this._monitor.y );

            // Remove styling over-ride applied to the panel
            ( SHELL_VERSION_MAJOR >= 42 ) ?
                this._panel.remove_style_class_name( this._comfortSettings.containerStyle ) :
                this._panel._removeStyleClassName( this._comfortSettings.containerStyle );

            // Delete our custom property inside of Main.panel
            delete this._panel.edge;
            this.active = false;
        }
    }
}
