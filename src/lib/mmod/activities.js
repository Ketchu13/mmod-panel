/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// DEFINES
const {
    St,
    Gio
}           = imports.gi;
const {
    main
}           = imports.ui;



class Modification {
    constructor( configuration ) {
        this._rigging = configuration.rigging;
        this._comfortSettings = configuration.comfortSettings;

        this._panelBox = null;
        this._panelButton = main.panel.statusArea.activities;
        this._panelButton._components = {
            icon: null,
            button: null
        };

        this._originalComponent = null;

        this._realize = null;

        this.active = false;

    }

    enable() {
        if( this._rigging.settings.get_boolean( 'panel-button-enabled' ) ) {
            this._panelBox = main.panel.actor.get_parent();

            // Handle realize if applicable
            if( !this._panelBox && !this._realize ) {
                // Shell/Extension has just initialized
                this._realize = this._panelBox.connect( 'realize', this.enable.bind( this ) );

                // Do not allow the method to continue
                return;
            }

            if( this._panelBox && this._realize ) {
                // This is the second invocation, called after realization of the panelBox
                this._panelBox.disconnect( this._realize );
                this._realize = false;
            }

            // The panel button like a windows 'start' button
            this._panelButton._components.button = new St.Bin(
                {
                    style_class: 'panel-button mmod-panel-button-style',
                    reactive: true,
                    can_focus: true,
                    //x_fill: false,
                    //y_fill: false,
                    track_hover: false
                }
            );

            // Replaces Activities Link
            this._panelButton._components.icon = new St.Icon(
                {
                    gicon: Gio.icon_new_for_string( this._rigging.settings.get_string( 'panel-button-icon-path' ) ),
                    style_class: this._comfortSettings.buttonStyle
                }
            );

            this._panelButton._components.button.set_child( this._panelButton._components.icon );

            this._originalComponent = this._panelButton.actor.get_children()[0];        // Get a handle on the original clutter text used for the link
            this._panelButton.actor.remove_child( this._originalComponent );            // Remove it
            this._panelButton.actor.add_child( this._panelButton._components.button );  // Replace it

            this.active = true;
        }
    }

    disable() {
        if( this.active ) {
            this._panelButton.actor.remove_child( this._panelButton._components.button );   // Remove our replacement
            this._panelButton.actor.add_child( this._originalComponent );                   // And replace the original

            if( this._panelButton._components.button ) {
                // Should get rid of our icon as well
                this._panelButton._components.button.destroy();
                this._panelButton._components.button = null;
                this._panelButton._components.icon = null;
            }

            if( this._originalComponent )
                this._originalComponent = null;

            this.active = false;
        }
    }
}
