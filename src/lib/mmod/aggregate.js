/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// DEFINES
const {
    St,
    Gio
}                       = imports.gi;
const {
    main,
    popupMenu
}                       = imports.ui;
const {
    extensionUtils
}                       = imports.misc;
const our               = extensionUtils.getCurrentExtension();


class Modification {
    constructor( configuration ) {
        this._rigging = configuration.rigging;

        this._aggregateMenuEntry = {
            components: {
                popup: null,
                menu: null,
                iconObject: null,
                iconImage: null
            }
        };

        this._connections = null;

        this.active = false;
    }

    enable() {
        if( this._rigging.settings.get_boolean( 'show-in-tray-menu' ) ) {
            // Create a new popup submenu as an entry within the aggregate menu
            this._aggregateMenuEntry.components.iconImage = Gio.icon_new_for_string( this._rigging.path + '/res/img/mico/mstar/mmod-logo-fin-24.png' );
            this._aggregateMenuEntry.components.iconObject = new St.Icon( { gicon: this._aggregateMenuEntry.components.iconImage, style_class: 'popup-menu-icon' } );
            this._aggregateMenuEntry.components.popup = new popupMenu.PopupSubMenuMenuItem( 'MMOD Panel' );
            this._aggregateMenuEntry.components.popup.actor.insert_child_at_index( this._aggregateMenuEntry.components.iconObject, 1 );

            // Create Submenu entries for the popup submenu we created as an entry within the aggregate menu
            this._aggregateMenuEntry.components.menu = new Object;

            this._aggregateMenuEntry.components.menu.preferences = new popupMenu.PopupMenuItem( 'Preferences' );
            this._aggregateMenuEntry.components.popup.menu.addMenuItem( this._aggregateMenuEntry.components.menu.preferences );

            this._aggregateMenuEntry.components.menu.feedback = new popupMenu.PopupMenuItem( 'Feedback' );
            this._aggregateMenuEntry.components.popup.menu.addMenuItem( this._aggregateMenuEntry.components.menu.feedback );

            // Register our event handlers
            this.connect();

            // Fix for 3.10+ ( https://github.com/hackedbellini/Gnome-Shell-Notifications-Alert/issues/18#issuecomment-28749827 )
            if( main.panel.statusArea.aggregateMenu !== undefined ) {
                let function_model = () => 1;
                // Sometimes the extension starts too fast and 9 hasn't loaded yet (i.e. Debian with a 6-core i7 and SSD,
                // while other times it just doesn't exist (i.e. Ubuntu)
                for( let i = 9; i >= 0; i-- ) {
                    if( main.panel.statusArea.aggregateMenu.menu._getMenuItems()[i] )
                        if( typeof( main.panel.statusArea.aggregateMenu.menu._getMenuItems()[i].addMenuItem ) === typeof( function_model ) ) {
                            main.panel.statusArea.aggregateMenu.menu._getMenuItems()[i].addMenuItem( new popupMenu.PopupSeparatorMenuItem(), 1 );
                            main.panel.statusArea.aggregateMenu.menu._getMenuItems()[i].addMenuItem( this._aggregateMenuEntry.components.popup, 2 );

                            i = -1;
                        }
                        else
                            log( '[MMOD Panel]: Could not find a point of entry to the Aggregate Menu.' );
                }
            }
            else {   // Since gnome-shell version 43.0  (https://gjs.guide/extensions/upgrading/gnome-shell-43.html#quick-settings)
                if( main.panel.statusArea.quickSettings !== undefined ) {
                    let function_model = () => 1;
                    // Sometimes the extension starts too fast and 9 hasn't loaded yet (i.e. Debian with a 6-core i7 and SSD,
                    // while other times it just doesn't exist (i.e. Ubuntu)
                    for( let i = 9; i >= 0; i-- ) {
                        if( main.panel.statusArea.quickSettings.menu._getMenuItems()[i] )
                            if( typeof( main.panel.statusArea.quickSettings.menu._getMenuItems()[i].addMenuItem ) === typeof( function_model ) ) {
                                main.panel.statusArea.quickSettings.menu._getMenuItems()[i].addMenuItem( new popupMenu.PopupSeparatorMenuItem(), 1 );
                                main.panel.statusArea.quickSettings.menu._getMenuItems()[i].addMenuItem( this._aggregateMenuEntry.components.popup, 2 );

                                i = -1;
                            }
                            else
                                log( '[MMOD Panel]: Could not find a point of entry to the Aggregate Menu.' );
                    }
                }
                else // Should work in 3.8
                    main.panel.statusArea.userMenu.menu.addMenuItem( this._aggregateMenuEntry.components.popup, 5 );
            }

            this.active = true;
        }
    }

    disable() {
        if( this.active ) {
            this.disconnect();

            if( this._aggregateMenuEntry.components.popup ) {
                this._aggregateMenuEntry.components.popup.destroy();
                this._aggregateMenuEntry.components.popup = null;
            }

            if( this._aggregateMenuEntry.components.menu ) {
                for( let i in this._aggregateMenuEntry.components.menu ) {
                    this._aggregateMenuEntry.components.menu[i].destroy();
                    this._aggregateMenuEntry.components.menu[i] = null;
                }

                this._aggregateMenuEntry.components.menu = null;
            }

            if( this._aggregateMenuEntry.components.icon ) {
                this._aggregateMenuEntry.components.icon.destroy();
                this._aggregateMenuEntry.components.icon = null;
            }

            if( this._aggregateMenuEntry.components.gicon )
                this._aggregateMenuEntry.components.gicon = null;

            this.active = false;
        }
    }

    /**
     * Method to register event handlers for the menu items
     */
    connect() {
        this._connections = [
            [
                this._aggregateMenuEntry.components.menu.preferences, this._aggregateMenuEntry.components.menu.preferences.connect( 'activate', this.loadPreferences.bind( this ) )
            ],
            [
                this._aggregateMenuEntry.components.menu.feedback, this._aggregateMenuEntry.components.menu.feedback.connect( 'activate', this.loadFeedback.bind( this ) )
            ]
        ];
    };

    /**
     * Method to remove event handlers for the menu items
     */
    disconnect() {
        if( this._connections ) {
            for( let i = 0; i < this._connections.length; i++ ) {
                this._connections[i][0].disconnect( this._connections[i][1] );
            }
            this._connections = null;
        }
    };

    /**
     * Method to attempt loading the extension preferences window for the
     * user
     */
    loadPreferences() {
        main.Util.trySpawnCommandLine( 'gnome-shell-extension-prefs ' + esys.metadata.uuid );
    };

    /**
     * Method to attempt loading the default browser and directing the user to
     * the mmogp.com home page
     *
     * @since 0.1.0
     */
    loadFeedback() {
        extensionUtils.openPrefs();
    };
}
