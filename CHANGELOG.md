# ChangeLog

## mmod-panel (13.0.0) stable; urgency=high

### Added

* `DEP5/copyright`
* `LICENSES/Apache-2.0.txt`

### Changed

* Updated extension in accordance with GJS Guidelines for Gnome-Shell v43.0
  * Reference: https://gjs.guide/extensions/upgrading/gnome-shell-43.html
* Updated favorites module to properly set SHELL_VER_3_3_OR_4 flag when run
  under Gnome-Shell v43.0
* Updated Position to properly detect Gnome-Shell version and apply appropriate
  case when enabled/disabled.
* Updated AggregateMenu module to apply modifications to QuickSettings when run
  under Gnome-Shell v43.0
  version 43.0
* Updated LICENSE.md with latest license information
  * Latest template provided, instructions included for determining the correct
    copyright and license information

### Removed

* Removed convenience from all library components

-- Richard B Winters <rik@mmod.co> Mon, 8 Nov 2021 10:15 -0500

## mmod-panel (12.0.0) stable; urgency=high

### Added

### Changed

* Updated extension to leverage latest GJS implementation
  * Updated all code to use ES6 syntax and language features
  * Library components all leverage class keyword and had Mod name
    changed to Modification.
  * Object and Array destructuring used for module importing and
    where appropriate
  * `lang` library utilization removed in favor of GJS built-in
    bind support
* Updated behavior modifications, renamed behaviors to represent
  the behavior modification provided
  * Autohide changed to `EnableAutoHideBehavior`
  * HotCorner changed to `DisableHotCornerBehavior`
* Prefs updated to reflect change to HotCorner behavior
  * Done to properly represent what the extension was doing. Instead
    of making it seem as though the extension enables hot corners, but
    can disable them - it appropriately represents that hot corners
    are default for the DE,yet can be disabled.
* Updated translations
  * Updated translation location references in `*.po` file.
  * Compiled new `*.mo` file.
* Updated version string to match range and scheme of the extension's
  version as represented on extensions.gnome.org.

### Removed

* Removed convenience from all library components

-- Richard B Winters <rik@mmod.co> Mon, 8 Nov 2021 10:15 -0500

## mmod-panel (1.3) stable; urgency=high

### Added

* Template UI file for future conversion to template-based preferences.

### Changed

* Fixed logic for testing for version support, where prior logic did not
  check that major version was less than `4`.
* Fixed support for GTK 4 in preferences when gnome-shell version is at
  least `4`.

### Removed

-- Richard B Winters <rik@mmod.co> Sat, 30 Oct 2021 16:38 -0500

## mmod-panel (1.2.2) stable; urgency=high

### Added

* Added Joel Denning to contributors list within `AUTHORS.md`

### Changed

* Fixed issue with panel position error when monitor configuration changes
  * Added global connections for handling said edge case in `extension.js`

### Removed

-- Richard B Winters <rik@mmod.co> Tue, 12 Jan 2021 10:38 -0500

## mmod-panel (1.2.1) stable; urgency=high

### Added

* Added Continuous Deployment to GitLab-CI Configuration

### Changed

* Updated GitLab-CI Configuration
  * Removed `before_script` as this runs before each job

### Removed

-- Richard B Winters <rik@mmod.co> Tue, 12 Jan 2021 5:48 -0500

## mmod-panel (1.2.2-10) stable; urgency=high

### Added

* Added Continuous Deployment to GitLab-CI Configuration

### Changed

* Updated GitLab-CI Configuration
  * Removed `before_script` as this runs before each job

### Removed

-- Richard B Winters <rik@mmod.co> Tue, 12 Jan 2021 5:48 -0500

## mmod-panel (1.2.1-10) stable; urgency=high

### Added

* Added support for Gnome-Shell 3.38.2
* Added GitLab-CI Configuration

### Changed

* Fixed `tweener` import location from `imports.ui.tweener` to `imports.tweener`
  * Applies when gnome-shell version >= 3.3
* Updated `st.Bin()` implementation to follow API change(s)
  * `x_fill` and `y_fill` were removed.
* Resolved issue with race conditions when panel position is set to `bottom`
  * Temporary fix, a more permanent solution is being investigated.

### Removed

-- Richard B Winters <rik@mmod.co> Mon, 11 Jan 2021 3:48 -0500
## mmod-panel (1.2.0-9) stable; urgency=high

### Added

* Added proper support for Gnome-Shell 3.30, 3.32, 3.34, 3.35, 3.35.91, 3.36, 3.36.3, 3.36.4, 3.36.6, 3.38

### Changed

* Updated version reference(s) in extension metadata
  * Version reference(s) also updated through out code base
* Updated favorites implementation to work with newer shell version(s)
* Updated attribution text across all files
* Updated README.md, INSTALL.md, to incorporate more complete documentation covering install/uninstall instructions
* Updated AUTHORS.md to reflect accurate updated upstream author email
* Updated gulpfile in keeping build system up-to-date and pristine
  * Updated package.json file with new scripts and build-system dependencies

### Removed

-- Richard B Winters <rik@mmod.co> Wed, 11 Apr 2018 3:25 -0500

## mmod-panel (1.1.1-8) stable; urgency=high

### Added

* Added proper support for Gnome-Shell 3.20, 3.22, 3.24, 3.26, 3.26.2, 3.28

### Changed

* Updated version reference in extension metadata
* Updated Author email for Richard B Winters
* Updated email for publishing company Massively Modified, Inc.
* Updated uuid due to email and domain change.

### Removed

-- Richard B Winters <rik@mmod.co> Wed, 11 Apr 2018 3:25 -0500

## mmod-panel (1.1.1-7) stable; urgency=high

### Added

* Added proper support for Gnome-Shell 3.18

### Changed

* Updated version reference in extension metadata

### Removed

-- Richard B Winters <rik@mmogp.com> Wed, 14 Oct 2015 23:21 -0500

## mmod-panel (1.1.0-6) stable; urgency=high

### Added

* Added proper support for Gnome-Shell 3.16 (favorites display fixed)

### Changed

* Updated version reference in extension metadata

### Removed

-- Richard B Winters <rik@mmogp.com> Wed, 24 Jun 2015 03:07 -0500

## mmod-panel (1.0.0-5) stable; urgency=medium

### Added

* Added link to our source repository in the extension description

### Changed

* Updated version reference in extension metadata
* Updated references to issues and source repository locations in preferences window
* Fixed issue [#4](https://code.mmogp.com/mmod/mmod-panel/issues/4)

### Removed

-- Richard B Winters <rik@mmogp.com> Fri, 20 Feb 2015 04:41 -0500

## mmod-panel (0.1.4) stable; urgency=low

### Added

### Changed

* Updated version, seeing if incremented version via minor version is allowed on extensinos.gnome.org
* Updated metadata.json to point to correct homepage which is now located at github.com
* Updated references to issues so they properly reflected those on github.com

### Removed

-- Richard B Winters <rik@mmogp.com>  Fri, 20 Feb 2015 04:40:14 -0500

## mmod-panel (0.1.3) stable; urgency=low

### Added

* Added Git ignore file, added instructions for ignoring Eclipse project files as well as the svn directory

### Changed

* Updated README to take advantage of GFM

### Removed

-- Richard B Winters <rik@mmogp.com>  Fri, 20 Feb 2015 02:46:32 -0500

## mmod-panel (0.1.2) stable; urgency=medium

### Added

* Added GNU project standard files; NEWS, README, INSTALL, CHANGELOG, AUTHORS, and COPYING
* Added German Translations provided courtesy of Jonius Zeidler <jonatan_zeidler@gmx.de>

### Changed

* Hotfix implemented for the installation error due to a bad method invocation on first-run. (Closes: #3)
* Hotfix implemented for the issue with undefined issue on Ubuntu systems. (Closes: #4)

### Removed

-- Richard B Winters <rik@mmogp.com>  Thurs, 22 Jan 2015 03:38:23 -0500

## mmod-panel (0.1.1) stable; urgency=medium

### Added

### Changed

* Hotfix implemented for initialization issues caused shell artifacts being undefined. (Closes: #2)

### Removed

-- Richard B Winters <rik@mmogp.com>  Sun, 30 Nov 2014 21:18:40 -0500

## mmod-panel (0.1.0) stable; urgency=low

* Initial release.

-- Richard B Winters <rik@mmogp.com>  Sun, 30 Nov 2014 21:16:08 -0500
